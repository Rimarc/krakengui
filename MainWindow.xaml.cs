﻿using KrakenApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YahooFinanceData;

namespace KrakenGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string krakenKey = KeyClass.krakenKey;
        private const string krakenSecret = KeyClass.krakenSecret;
        public Kraken kraken;

        public Dictionary<string, AssetPair> Pairs;
        public Dictionary<string, AssetPair> MarginEnabledPairs;
        public string MarginEnabledPairsString;
        public List<Dictionary<string, Ticker>> AssetPairHistory = new List<Dictionary<string, Ticker>>();

        public MainWindow()
        {
            InitializeComponent();
            kraken = new Kraken(krakenKey, krakenSecret, 2500);
            Pairs = kraken.GetAssetPairs();
            MarginEnabledPairs = Pairs.Where(ele => ele.Value.LeverageBuy.Length != 0).ToDictionary(t => t.Key, t => t.Value);
            MarginEnabledPairsString = MarginEnabledPairs.Aggregate("", (a, b) => a + (a == "" ? "" : ",") + b.Key);

            CreatePairTreeView();
        }

        private void CreatePairTreeView()
        {
            foreach (var ele in Pairs.OrderByDescending(a => a.Value.LeverageBuy.Length))
            {
                TreeViewItem NewItem = new TreeViewItem();
                NewItem.Header = ele.Value.Altname + (ele.Value.LeverageBuy.Length == 0 ? "" : ("[" + ele.Value.LeverageBuy.Max() + "]"));
                NewItem.Tag = ele;
                AssetTreeView.Items.Add(NewItem);
                NewItem.Items.Add(ele.Key);

                var ContextMenu = new ContextMenu();
                var CreateOrderMenuItem = new MenuItem();
                CreateOrderMenuItem.Click += CreateOrderMenuItem_Click;
                CreateOrderMenuItem.Header = "New Order";
                CreateOrderMenuItem.Tag = ele.Key;
                ContextMenu.Items.Add(CreateOrderMenuItem);
                NewItem.ContextMenu = ContextMenu;
            }
        }

        private void CreateOrderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new NewOrderWindow((string)((MenuItem)sender).Tag, this).Show();
        }

        private async void BalanceUpdateButton_Click(object sender, RoutedEventArgs e)
        {

            var BalanceUpdateTask= Task.Factory.StartNew(() => kraken.GetAccountBalance());
            
            BalanceMainStackPanel.Children.Clear();
            BalanceMainStackPanel.Children.Add(new Label { Content="Fetching Data"});

            await BalanceUpdateTask;

            foreach (var assets in BalanceUpdateTask.Result)
            {
                BalanceMainStackPanel.Children.Add(new Label { Content = assets.Key + ": " + assets.Value });
            }

            var TradeBalanceUpdateTask= Task.Factory.StartNew(() => kraken.GetTradeBalance());
            await TradeBalanceUpdateTask;
            BalanceMainStackPanel.Children.Add(new Label { Content = TradeBalanceUpdateTask.Result.Equity.ToString()});

        }

        private void PositionUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            //TT4FJK-GS54B-MFQOLA

            var openPositions = kraken.GetOpenPositions(new string[] { "TT4FJK-GS54B-MFQOLA"}, true);

            PositionMainStackPanel.Children.Clear();
            foreach (var pair in openPositions.GroupBy(ele=>ele.Value.Pair))
            {
               
                //PositionMainStackPanel.Children.Add(new Label { Content = Pairs[pair.Key].Altname + " " + sum+" "+PL, Foreground= pair.First().Value.Type=="buy"?Brushes.DarkGreen:Brushes.Red });
                PositionMainStackPanel.Children.Add(new PosionControl(pair, this));
            }
        }

        private async void OpenOrdersUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            var openOrdersTask = Task.Factory.StartNew(() => kraken.GetOpenOrders());

            ((Button)sender).IsEnabled = false;

            OpenOrdersMainStackPannel.Children.Clear();
            OpenOrdersMainStackPannel.Children.Add(new Label { Content = "Fatching Data" });


            

            await openOrdersTask;

            OpenOrdersMainStackPannel.Children.Clear();
            foreach (var assets in openOrdersTask.Result)
            {
                var orderInfo = assets.Value;
                OpenOrdersMainStackPannel.Children.Add(new OpenOrderControl(assets, this));

            }

            ((Button)sender).IsEnabled = true;

        }

        private void MarginAssetsUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            

            var prices = kraken.GetTicker(MarginEnabledPairsString);
            AssetPairHistory.Add(prices);

            MarginAssetsMainStackPannel.Children.Clear();
            foreach (var pair in MarginEnabledPairs.OrderByDescending(ele=>ele.Value.LeverageBuy.Length))
            {
                MarginAssetsMainStackPannel.Children.Add(new Label { Content = pair.Value.Altname 
                    + " " 
                    + (pair.Value.LeverageBuy.Length>0?pair.Value.LeverageBuy.Select(x => x.ToString()).Aggregate((a,b)=> a+" "+b) : ""  ) 
                    + " " 
                    +prices[pair.Key].Bid[0]
                +" "
                +(((AssetPairHistory[0][pair.Key].Bid[0] - prices[pair.Key].Bid[0])/ prices[pair.Key].Bid[0])*100)
                });
            }
        }

        private void ArbitageUpdateButton_Click(object sender, RoutedEventArgs e)
        {
            var tickers = kraken.GetTicker(AssetPairsIdentifier.BTCEUR + "," + AssetPairsIdentifier.BTCUSD + "," + AssetPairsIdentifier.ETHEUR + "," + AssetPairsIdentifier.ETHUSD + "," + AssetPairsIdentifier.ETCEUR + "," + AssetPairsIdentifier.ETCUSD);
            ArbitageStackPanel.Children.Clear();

            double forexPrice = EasyTicker.GetTicker(EasyTicker.EURUSD);
            CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.BTCUSD, AssetPairsIdentifier.BTCEUR);
            CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.ETHUSD, AssetPairsIdentifier.ETHEUR);

            CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.ETCUSD, AssetPairsIdentifier.ETCEUR);

            ArbitageStackPanel.Children.Add(new Label { Content = forexPrice.ToString() });
        }

        private void CalcArbOp(Dictionary<string, Ticker> tickers, double forexPrice, string firstPair, string secondPair)
        {

            //Mit dollar kaufen
            //Mit euro verkaufen
            //grün = AB kleiner forexprice -> euro besonders wenig kaufkraft-> mehr als normal euro pro btc -> Vorteilhaft btc zu leihen und zu verkaufen für euro. Mit USD kaufen um zu hedgen
            double AB = (double)(tickers[firstPair].Ask[0] / tickers[secondPair].Bid[0]);

            //Mit dollar verkaufen
            //Mit euro kaufen
            //grün = BA größer forexprice -> euro beonders viel kaufkraft -> weniger als normal euro pro btc -> Vorteilhaft euro zu leihen und btc zu kaufen. Mit USD hedgen.
            double BA = (double)(tickers[firstPair].Bid[0] / tickers[secondPair].Ask[0]);

            //Below forex
            ArbitageStackPanel.Children.Add(new Label
            { Content = firstPair+"/"+secondPair});
            ArbitageStackPanel.Children.Add(new Label { Content = "AB: " + AB, Foreground = forexPrice < AB ? Brushes.Red : Brushes.DarkGreen });
            ArbitageStackPanel.Children.Add(new Label { Content = "BA: " + BA, Foreground = forexPrice > BA ? Brushes.Red : Brushes.DarkGreen });
        }
    }
}
