﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KrakenApi;

namespace KrakenGUI
{
    /// <summary>
    /// Interaction logic for OpenOrderControl.xaml
    /// </summary>
    public partial class OpenOrderControl : UserControl
    {
        private KeyValuePair<string, OrderInfo> assets;
        private MainWindow mainWindow;

        private string OrderID;

        public OpenOrderControl()
        {
            InitializeComponent();
        }

        public OpenOrderControl(KeyValuePair<string, OrderInfo> assets, MainWindow mainWindow):this()
        {
            this.assets = assets;
            this.mainWindow = mainWindow;

            OrderID = assets.Key;

            var PairName = assets.Value.Descr.Pair;

            this.PairName.Content = PairName;

            this.Positionsize.Content = assets.Value.Volume;
            this.PositionsizeLeft.Content = (assets.Value.Volume - assets.Value.VolumeExecuted);
            //OpenOrdersMainStackPannel.Children.Add(new Label { Content = assets.Key + "[" + orderInfo.Status + "] " + orderInfo.VolumeExecuted + " / " + orderInfo.Volume });
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.kraken.CancelOrder(OrderID);
        }
    }
}
