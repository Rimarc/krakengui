﻿using KrakenApi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KrakenGUI
{
    /// <summary>
    /// Interaction logic for NewOrderWindow.xaml
    /// </summary>
    public partial class NewOrderWindow : Window
    {
        string pairIdentifier;
        AssetPair pair;
        MainWindow mainWindow;
        public NewOrderWindow()
        {
            InitializeComponent();
        }

        public NewOrderWindow(string pairIdentifier, MainWindow mainWindow):this()
        {
            this.pairIdentifier = pairIdentifier;
            this.pair = mainWindow.Pairs[pairIdentifier];
            PairIdentifierLabel.Content = pair.Altname;
            this.mainWindow = mainWindow;

            this.MarginCheckBox.Visibility= pair.LeverageBuy.Length != 0? Visibility.Visible:Visibility.Hidden;
        }

        private void SendOrderButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO sanity...use margin depending of buy sell uso
            //MessageBox.Show((this.isShortCheckBox.IsChecked.Value?"Sell ":"Buy ") + SizeInput.Text + " of " + pair.Altname);
            var Order = new KrakenOrder();
            if (pair.LeverageBuy.Length != 0 && MarginCheckBox.IsChecked.HasValue && MarginCheckBox.IsChecked.Value)
            {
                Order.Leverage = pair.LeverageBuy.Max();
            }
            Order.Pair = pairIdentifier;
            Order.Type = ((bool)isShortCheckBox.IsChecked)?"sell":"buy";
            Order.Volume = decimal.Parse(SizeInput.Text);
            if (MarketOrderCheckBox.IsChecked.Value)
            {
                Order.OrderType = "market";
            }
            else
            {
                Order.OrderType = "limit";
                Order.Price = decimal.Parse(PriceTextBox.Text, CultureInfo.InvariantCulture);
            }
            Order.Validate = true;
            var res = mainWindow.kraken.AddOrder(Order);
            var erg = MessageBox.Show(res.Descr.Order, "Preview of new Order", MessageBoxButton.YesNo);
            if(erg==MessageBoxResult.No)
            {
                //Discards
                return;
            }
            Order.Validate =null;
            res = mainWindow.kraken.AddOrder(Order);
            MessageBox.Show("Order Added succesfull");

        }

        private void MarketOrderCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PriceTextBox.Visibility = Visibility.Visible;
        }

        private void MarketOrderCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            PriceTextBox.Visibility = Visibility.Hidden;
        }

        bool FirstTimeFocusSize = false;
        private void SizeInput_GotFocus(object sender, RoutedEventArgs e)
        {
            if(FirstTimeFocusSize==false)
            {
                SizeInput.Text = "";
            }
            FirstTimeFocusSize = true;
        }

        bool FirstTimeFocusPrice = false;
        private void PriceTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (FirstTimeFocusPrice == false)
            {
                PriceTextBox.Text = "";
            }
            FirstTimeFocusPrice = true;
        }
    }
}
